from django.db import models
from django.conf import settings

class Device(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название камеры')
    is_camera = models.BooleanField(default=False)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Пользователь', related_name='device_user')
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Alarm(models.Model):
    device = models.ForeignKey('Device', on_delete=models.CASCADE, verbose_name='Устройство')
    type_alarm = models.ForeignKey('AlarmType', on_delete=models.CASCADE, blank=True, verbose_name='Тип тревоги')
    date_alarm = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    is_watched = models.BooleanField(default=False)
    file = models.ImageField("Фото", upload_to="alarms/")


class AlarmType(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название тревоги')

    def __str__(self):
        return self.name